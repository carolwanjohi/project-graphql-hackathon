import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { map } from 'rxjs/operators';
import gql from 'graphql-tag';
import { PostInterface, Query } from '../type';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  allPosts: Observable<PostInterface[]>;

  constructor(
    private apollo: Apollo
  ) { }

  ngOnInit() {
    this.allPosts = this.apollo.watchQuery<Query>({
      query: gql`
     query {
        allPosts{
          edges{
            node{
              title
              id
              body
            }
          }
        }
      }
      `
    })
    .valueChanges
    .pipe(
      map(result => { 
        console.log('result', result)
      return  result.data.allPosts
      })
    )
  }

}
