export interface PostInterface {
 id?: string;
 title?: string;
 body?: string;
}

export type Query = {
    allPosts: PostInterface[];
}